import socket
import random

# Lista de URLs
url_list = [
    "https://www.twitch.tv",
    "https://github.com",
    "https://example.net",
    "https://gitlab.eif.urjc.es",
    "https://www.youtube.com"
]

myPort = 1234

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as mySocket:
    mySocket.bind(('', myPort))

    mySocket.listen(5)
    try:
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            with recvSocket:
                print("HTTP request received:")
                print(recvSocket.recv(2048))
                nextPage = str(random.randint(0, 10000))
                nextUrl = "http://localhost:" + str(myPort) + "/" + nextPage
                url_list.append(nextUrl)
                url = random.choice(url_list)
                if url == nextUrl:
                    response = "HTTP/1.1 302 Found\r\n" + "Location: " + url + "\r\n\r\n"
                    recvSocket.send(response.encode('utf-8'))
                else:
                    response = "HTTP/1.1 302 Found\r\n" + "Location: " + url + "\r\n\r\n"
                    recvSocket.send(response.encode('utf-8'))

    except KeyboardInterrupt:
        print("Closing binded socket")
        mySocket.close()
